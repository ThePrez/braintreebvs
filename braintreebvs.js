var braintree = require("braintree");
var http = require("http");
var util = require("util");
var db = require("idb-connector");
var fs = require("fs");
var path = require("path");

var configFile = path.resolve(__dirname, "config.json");
var counterFile = path.resolve(__dirname, "counter.json");
var myOutputRoot = path.resolve(__dirname, "logs");

//Main Function
function processWebhook(req, callback) {
  var plist = {config : {},
               environment : braintree.Environment.Sandbox,
               uniqueID : 0,
               retryCount : 0, 
               errorMessages : [],
               totalTransactionsProcessed : 0,
               transactionsProcessed : 0,
               totalTransactionsReceived : 0,
               transactionsReceived : 0,
               transactionErrors : [],
               requestID : "none"};
            
  var errorText = "";


  plist.config = JSON.parse(fs.readFileSync(configFile, "utf8"));

  if (plist.config.environment == "production") {
    plist.environment = braintree.Environment.Production;    
  }

  var gateway = braintree.connect({
    environment: plist.environment,
    merchantId: plist.config.myMerchantID,
    publicKey: plist.config.myPublicKey,
    privateKey: plist.config.myPrivateKey
  });

  plist.uniqueID = getNextID();
  writeStatus(plist,'Environment: ' + JSON.stringify(plist.environment.server));
  
  var arr = req.bt_signature.split("|");

  if (arr.length > 1) {
    plist.requestID = arr[1];
    writeDiagnostic(plist, "Request " + plist.requestID + " received.");
  }
  
  if ((gateway) && (plist.uniqueID > 0)) {

    if (plist.config.logRequest) {
      writeToFile(plist, myOutputRoot + "/request_" + plist.uniqueID + ".req", JSON.stringify(req));
    }      

    gateway.webhookNotification.parse(req.bt_signature, req.bt_payload, function (err, webhookNotification) {
      
      if (plist.config.logDisbursement) {
        var xmlString = Buffer.from(req.bt_payload, 'base64'); 
        writeToFile(plist, myOutputRoot + "/disbursement_" + plist.uniqueID + ".xml", xmlString);   
      }
      
      if (webhookNotification) {

        // Distribution 
        if (webhookNotification.kind == braintree.WebhookNotification.Kind.Disbursement) {
            writeStatus(plist, 'Disbursement: ' + webhookNotification.disbursement.id);
            writeDisbursementData(plist, gateway, webhookNotification, callback);
        } 
        
        // Subscription Charged Successfully
        else if (webhookNotification.kind == braintree.WebhookNotification.Kind.SubscriptionChargedSuccessfully) {
            writeStatus(plist, 'Subscription Charged Successfully: ' + webhookNotification.subscription.id);
            writeSubscriptionChargedSuccessfullyData(plist, webhookNotification, callback);
        } 

        // Unknown or unmonitored kind
        else {
            writeError(plist, "Unmonitored webhook kind of '" + webhookNotification.kind + "' encountered.");
            finish(plist, callback);
        }
        
      } else {
        errorText = "Request ID " + requestID + ":" + err;
        writeError(plist, errorText);
        finish(plist, callback);
      }

    });
    
  } else {
    
    if (! gateway) {
      errorText = "Gateway error.";
    }
    
    if (plist.uniqueID < 0) {
      errorText = "Unique ID generation error";
    }
    
    writeError(plist, errorText);
    finish(plist, callback);
  }
  
}

//Write Disbursement Data
function writeDisbursementData(plist, gateway, notification, callback) {
  writeWebhookLog(plist, notification);

  //var transactionList  = ['54qqagzy','cvmg1ptj','40dk1czzx','qp87t4vsx','244me62b','7nw5fd41','n8ryvbsa','1csn8h5p','kcjrcdj9','2jkx2mc9','kp4nv2xj','00849wv4'];
  //var transactionList  = ['54qqagzy','40dk1czz'];
  //var transactionListFile = path.resolve(__dirname, "sampletransactions.json");
  //var transactionList = fs.readFileSync(transactionListFile, "utf8").toString().split(",");
  var transactionList = notification.disbursement.transactionIds;

  var numberOfRetries = 0;
  plist.totalTransactionsReceived = transactionList.length;

  var task = function () {
    processTransactionArray(plist, gateway, transactionList, notification, function() {

      if ((plist.transactionErrors.length > 0) && (numberOfRetries++ < plist.config.transactionRetries)) {
        writeDiagnostic(plist, "We had an error... retry number " + numberOfRetries);
        transactionList  = plist.transactionErrors;
        task();
      } else {
        writeDisbursementRecord(plist, notification);
        finish(plist, callback);
      }
      
    });
  }
  
  task(); 
   
}

//Subscription Charged Successfully 
function writeSubscriptionChargedSuccessfullyData(plist, notification, callback) {
  writeWebhookLog(plist, notification);
  finish(plist, callback);
}

// Process a Transaction Array
function processTransactionArray(plist, gateway, transactionList, notification, callback) {
  plist.transactionsReceived = transactionList.length;
  plist.transactionsProcessed = 0;
  plist.transactionErrors = [];

  writeDiagnostic(plist, "Processing a total of " + plist.transactionsReceived + " transactions.");

  transactionList.forEach(function(transaction) {
    processTransaction(plist, gateway, transaction, notification, function() {

      if (plist.transactionsProcessed == plist.transactionsReceived) {
        callback();
      }

    });
  
  });

}

//Process a Single Transaction
function processTransaction(plist, gateway, transaction, notification, callback) {

  gateway.transaction.find(transaction, function (err, thisTransaction) {
    writeDiagnostic(plist, "Processing transaction: " + transaction + "...");
    
    if (err) {
      writeError(plist, "Transaction " + transaction + " error encountered for disbursement " + notification.disbursement.id + ".\nType: " + err.type + "\nName: " + err.name + "\nMessage: " +  err.message);
      plist.transactionErrors.push(transaction);
    } else {
      writeDiagnostic(plist, "Transaction " + transaction + " completed.");
      writeDisbursementTransactionRecord(plist, notification, thisTransaction);
      
      if (plist.config.logTransactions) {
        writeToFile(plist, myOutputRoot + "/disbursement_" + plist.uniqueID + "_" + transaction + ".json", JSON.stringify(thisTransaction));
      }
    }
    
    plist.totalTransactionsProcessed++;
    plist.transactionsProcessed++;
    callback();
  });

}

//Write to the Webhook Log File
function writeWebhookLog(plist, notification) {
  var sql = "INSERT INTO " + plist.config.myWorkingLibrary + ".BTLOGPF (BLID, BLREQID, BLKIND, BLOUTROOT) VALUES(%d,'%s','%s','%s') with none";
  sql = util.format(sql, plist.uniqueID, plist.requestID, notification.kind, myOutputRoot);
  runSQL(plist, sql);
}

//Write to the Disbursement Transaction Record File
function writeDisbursementTransactionRecord(plist, notification, transaction) {
  var transactionSource = "unknown";
  var settlementAmount = 0;
  
  for (i in transaction.statusHistory) {
  
    if (transaction.statusHistory[i].status == "settled") {
      settlementAmount = transaction.statusHistory[i].amount;
    }

    if (transactionSource == "unknown") {
      transactionSource = transaction.statusHistory[i].transactionSource;
    }      
  
  }
 
  var sql = "INSERT INTO " + plist.config.myWorkingLibrary + ".BTTRANPF (BTID, BTTRNID, BTTRNTYP, BTTRNSTS, BTTRNSRC, BTREFTID, BTORDERID, BTCCL4, BTAUTHCODE, BTTRNAMT, BTSETAMT) VALUES(%d,'%s','%s','%s','%s','%s','%s','%s','%s',%d, %d) with none";
  sql = util.format(sql, plist.uniqueID, transaction.id, transaction.type, transaction.status, transactionSource, transaction.refundedTransactionId, transaction.orderId, transaction.creditCard.last4, transaction.processorAuthorizationCode, transaction.amount, settlementAmount);
  runSQL(plist, sql);
 
}

//Write a Disbursement Record
function writeDisbursementRecord(plist, notification) {
  var processFlag = "N";

  if (plist.transactionErrors.length > 0) {
    processFlag = "E";
    writeDiagnostic(plist, "Writing " + plist.transactionErrors.length + " blank records to transaction file.");
    
    //write blank transaction records for errors
    plist.transactionErrors.forEach(function(transaction) {
      var sql = "INSERT INTO " + plist.config.myWorkingLibrary + ".BTTRANPF (BTID, BTTRNID) VALUES(%d,'%s') with none";
      sql = util.format(sql, plist.uniqueID, transaction);
      runSQL(plist, sql);
    });
    
  }

  writeDiagnostic(plist,"Writing disbursement record for " + plist.totalTransactionsReceived + " transactions with code of " + processFlag + ".");

  var sql = "INSERT INTO " + plist.config.myWorkingLibrary + ".BTDISBPF (BDID, BDDISID, BDDISAMT, BDDISDT, BDPRCFLG) VALUES(%d,'%s',%d,TO_DATE('%s','YYYY-MM-DD'),'%s') with none";
  sql = util.format(sql, plist.uniqueID, notification.disbursement.id, notification.disbursement.amount, notification.disbursement.disbursementDate, processFlag);
  runSQL(plist, sql);
}

function writeToFile(parmData, file, data) {
  fs.writeFile(file, data, 'utf-8', function(e) {
    if(e) {
      return writeError(parmData, "Error creating log file:" + e);
    }
    //success
  });
}

function runSQL(plist, sql) {

  var dbconn = new db.dbconn();
  dbconn.conn("*LOCAL");
  var stmt = new db.dbstmt(dbconn);
  stmt.exec(sql, function(rs, err) {
    stmt.close();                                    
    dbconn.disconn();  
    dbconn.close(); 

    if (err) {
      writeError(plist, "Error running SQL: " + sql + ":" + err);
    }
    
  });

}

function writeDiagnostic(plist, message) {

  if (! plist.config.silentMode) {
    console.log("[ID:" + plist.uniqueID + "]: " + message);
  }    

}

function writeStatus(plist, message) {
  console.log("[ID:" + plist.uniqueID + "]: " + message);
}

function writeError(plist, message) {
  console.log("[ID:" + plist.uniqueID + "]: **ERROR:" + message);  
  plist.errorMessages.push(message);
}

function finish(plist, callback) {
  var recovered = "";
  
  if (plist.transactionErrors.length > 0) {
    sendErrorEmail(plist);
  }

  if ((plist.errorMessages.length > 0) && (plist.transactionErrors.length == 0)) {
    recovered = " (recovered)";
  }  
  
  writeStatus(plist, plist.totalTransactionsReceived + " transactions received.");
  writeStatus(plist, plist.totalTransactionsProcessed + " transactions processed.");
  writeStatus(plist, plist.transactionErrors.length + " transaction errors.");
  writeStatus(plist, plist.errorMessages.length + " errors logged." + recovered);
 
  plist.errorMessages = [];
  callback();
}    

function sendErrorEmail(plist) {
  var toAddress = plist.config.recipientEmails;
  var subject = plist.config.system + " ALERT";
  var emailMessage =  JSON.stringify(plist.environment) + "\r\n\r\n";
  var recovered = "";

  emailMessage += "[ID:" + plist.uniqueID + "] The following message(s) were received from " + plist.config.system + ":\r\n\r\n";
  plist.errorMessages.forEach(function(thisMessage) {
    emailMessage += thisMessage += "\r\n\r\n";
  });

  if ((plist.errorMessages.length > 0) && (plist.transactionErrors.length == 0)) {
    recovered = " (recovered)";
  }  
  
  emailMessage += "\r\n\r\n";
  emailMessage += plist.totalTransactionsReceived + " transactions received.\r\n";
  emailMessage += plist.totalTransactionsProcessed + " transactions processed.\r\n";
  emailMessage += plist.transactionErrors.length + " transaction errors.\r\n";
  emailMessage += plist.errorMessages.length + " errors logged.\r\n" + recovered;

  var submitData = JSON.stringify({key:plist.config.mltsvckey, toAddress:toAddress, subject:subject, message:emailMessage});
  
  var options = {
    host: 'ws.bvstools.com',
    port: '80',
    path: '/webservice/mailtoolService',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded '
    }
  };
  
  var req = http.request(options, function(httpReq) {
    var msg = '';
  
    httpReq.setEncoding('utf8');
    httpReq.on('data', function(chunk) {
      msg += chunk;
    });
    
    httpReq.on('end', function() {
      var jsonContent;  
      var success;
      var response;
      
      try {
        jsonContent = JSON.parse(msg);  
        success=jsonContent.success;
        response=jsonContent.response;
      } catch(e) {
        success='false';
        response='Error parsing response: ' + e;
      }
      
      writeDiagnostic(plist, "@@ Email success: " + success + ":" + response);    
    });
  });
  
  req.write(submitData);
  req.end();
}

function getNextID() {
  var obj = JSON.parse(fs.readFileSync(counterFile, "utf8"));
  obj.uniqueID++;
  fs.writeFileSync(counterFile, JSON.stringify(obj));
  return obj.uniqueID;
}

function wait(ms) {
    var start = Date.now(),
        now = start;
    while (now - start < ms) {
      now = Date.now();
    }
}

module.exports = {
  processWebhook: processWebhook
}
