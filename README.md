# braintreebvs

Process a Braintree webhook

You should follow the Braintree NodeJS setup instructions first at:
https://developers.braintreepayments.com/start/hello-server/node

The braintreebvs node module and configuration files should be installed in the ../node_modules/braintreebvs subdirectory.

When updating make sure to make backups of your config and counter files as well as any logs.

## Usage

```javascript
var btbvs = require('braintreebvs')
```
## Methods

### btbvs.processWebhook(jsonBody)

Process a Braintree request.  

jsonBody should be the body of the request as a JSON object.

http example:

```javascript   
var body = "";
var error = "";
var jsonObj;

req.on('data', function (chunk) {
    body += chunk;
});

req.on('end', function() {
  if ((req.headers["content-type"]) == "application/x-www-form-urlencoded") {
    jsonObj = qs.parse(body);
  } else if ((req.headers["content-type"]) == "application/json") {
    jsonObj = JSON.parse(body);
  } else {
    error = "Invalid content type " + req.headers["content-type"];
    console.log(error);
    res.writeHead(500, {"Content-type":"text/plan"});
    res.end("Invalid request: " + error);
  }

  if  (jsonObj) {
    btbvs.processWebhook(jsonObj);
  }
``` 
     
This process will place data into a library on the IBM i regarding the webhook.

Right now only disbursements are processed.  For each disbursement a record is written
with a unique ID to file BTDISBPF.  For each transaction in the disbursement a record
is written to the BTTRANPF file, keyed also by the unique ID.

If any errors are found an email will be sent to the email(s) in the config file.

## IBM i Library

Save File btreebvs.sav contains a library with required IBM i information.  You can restore 
this as is (a library named BTREEBVS) or restore the objects to the library of your choice.

### BTLOGPF
This file will have a log for each webhook fired.

### BTDISBPF
This file will have an entry for each disbursement that was processed.  The process code will 
be "N" for success and "E" for a processing error.  This field is so you you can mark records
as processed when you are done with them.

Records are written to this file AFTER all transactions are written.  This is so you can add a 
trigger to this file to fire off processing of a disbursement.

### BTTRANPF
This file will have an entry for each transaction that was processed.  If there was an error retrieving
a transaction, a record will be written with blank information with the transaction ID.

## Configuration File

config.json should be located in the module directory:

```javascript 
{
  "environment" : "sandbox",
  "myWorkingLibrary" : "BTREEBVS",
  "myPublicKey" : "xxxxxxxxxxxxxxxx",
  "myMerchantID" : "xxxxxxxxxxxxxxxxx",
  "myPrivateKey" : "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  "mltsvckey" : "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  "recipientEmails" : ["you@you.com"],
  "system" : "Braintree Webhook TEST System",
  "logRequest" : true,
  "logDisbursement" : true,
  "logTransactions" : false,
  "silentMode" : false,
  "transactionRetries" : 3  
}
```
environment = Running environment.  Replace with either "sandbox" or "production" for your environment.

myWorkingLibrary = The IBM i library where you restored the BTREEBVS objects.

myPublicKey, myMerchantID, and myPrivateKey = Appropriate information regarding 
your Braintree account.  Keep in mind that your sandbox and live account credentials are different.

mltsvckey = A key acquired from www.bvstools.com.  This allows use of the MAILTOOL service to send emails.

recipientEmails = The email(s) you want to receive notifications via email.  Emails will come from
bvstools.com unless you give us information on what to use for your sending account.
You of course are not required to use this email service.  But that means you'll need to update the code to use
whatever service you want.

system = system description

logRequest = log the request or not.  ../logs/request_[id].req

logDisbursement = log the disbursement data or not.  ../logs/disbursement_[id].xml

logTransactions = log each transaction retrieved or not.  ../logs/disbursement_[id]_[transactionid].json

silentMode = Turn off all diagnostic logging or not

transactionRetries = How many times it will retry when there are errors retrieving transactions

## Counter File

counter.json is a file required to generate a unique and sequential id for each request received.  
This file should be located in the module directory.
